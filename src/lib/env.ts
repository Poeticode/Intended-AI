import * as dotenv from 'dotenv';
const config = dotenv.config();
const OPENAI_API_KEY = config.OPENAI_API_KEY;

export { OPENAI_API_KEY };
