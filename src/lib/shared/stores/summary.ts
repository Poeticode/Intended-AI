import { writable } from 'svelte/store';

const summary = writable<string>(JSON.stringify([]));

export default summary;
