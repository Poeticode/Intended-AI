declare module 'dotenv' {
	function config(options?: DotenvParseOptions): { [name: string]: string };
}
